#!/usr/bin/python3

from PyMvnReleaser.main import Main
import argparse
import logging

def run():
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', action='store', dest='config',
                        help='file de configuracion',required=True)
    main = Main(parser.parse_args().config)

    main.buildRoot()

if __name__ == "__main__":
   run()