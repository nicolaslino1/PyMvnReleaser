import git
from os import remove,chdir
from tempfile import mkdtemp
import logging

class Git():

    def __init__(self,repoUrl):
        self.path = self.__makeRandomDir()
        self.repoUrl = repoUrl
        self.git = git.Git()
        self.gitClone()

    def __enter__(self):
        return self

    def __exit__(self):
        remove(self.path)

    def gitClone(self):
        logging.info(msg="clonando: "+self.repoUrl)
        self.git.clone(self.repoUrl,self.path)

    def gitCommit(self):
        try:
            chdir(self.path)
            logging.info(msg="commiteando: ")
            self.git.commit('.', '-m', 'Version_autocommit')
        except:
            logging.error(msg="No changes to commit")

    def gitPush(self):
        try:
            chdir(self.path)
            logging.info(msg="pusheando")
            self.git.push('origin','master')
        except:
            logging.error(msg="No changes to push")

    @staticmethod
    def __makeRandomDir():
        return mkdtemp()

    def getPath(self):
        return self.path



