from xml.dom import minidom
from xml.parsers.expat import ExpatError
import re
import logging
import sys
class Pom(object):
    '''PASS'''

    def __init__(self,path):
        try:
            self.pomPath = str(path) + '/pom.xml'
            self.pom = minidom.parse(self.pomPath)
            self.artifactId = self.__setArtifactId()
            self.goupId = self.__setGroupId()
            self.dependencies = self.__setDependencies()
            self.type = self.__setType()
            self.artifactVersion = self.__setArtifactVersion()
            if self.type == 'pom':
                self.modules = self._setModules()
        except ExpatError:
            logging.error("Invalid xml format")
        except (FileExistsError, FileNotFoundError ,):
            logging.error("Macho, esto no tiene ningun porm")
        except:
            logging.error("Unexpected error:", sys.exc_info()[0])

    def __setArtifactId(self):
        artifactId = self.pom.getElementsByTagName("artifactId")[0]
        return artifactId.firstChild.data

    def getArtifactId(self):
        return self.artifactId

    def __setArtifactVersion(self):
        artifactVersion = self.pom.getElementsByTagName("version")[0]
        return artifactVersion.firstChild.data

    def getArtifactVersion(self):
        return self.artifactVersion

    def __setGroupId(self):
        artifactId = self.pom.getElementsByTagName("groupId")[0]
        return artifactId.firstChild.data

    def getGroupId(self):
        return self.goupId

    def __setDependencies(self):
        deps = self.pom.getElementsByTagName("dependency")
        dependencies = dict()

        pattern = re.compile("\$\{")
        patternProject = re.compile("project")
        patternSnap = re.compile(".*-SNAPSHOT")

        for dep in deps:
            dic = dict()
            dic["groupId"] = dep.getElementsByTagName("groupId")[0].firstChild.data
            artifactId = dep.getElementsByTagName("artifactId")[0].firstChild.data
            try:
                dic["type"] = dep.getElementsByTagName("type")[0].firstChild.data
            except:
                dic["type"] = 'jar'

            version = dep.getElementsByTagName("version")[0]
            if patternSnap.match(version.firstChild.data):
                version.firstChild.replaceWholeText(str(version.firstChild.data).replace('-SNAPSHOT',''))
                dic['version'] = str(version.firstChild.data).replace('-SNAPSHOT','')
            else:
                dic['version'] = version.firstChild.data

            if pattern.match(dic['version']) and not patternProject.findall(dic['version']):
                dic['version'] = self.__getPropertyVersion(re.findall(r"[\w,-]+", dic['version'])[0])

            # se quitan los hijos
            if not patternProject.findall(dic['version']):
                dependencies[artifactId] = dic

        self.pom.writexml(open(self.pomPath,'w'))
        return(dependencies)

    def getDependencies(self):
        return self.dependencies

    def __getPropertyVersion(self, tag):
        patternSnap = re.compile(".*-SNAPSHOT")
        properties = self.pom.getElementsByTagName("properties")[0]
        property = properties.getElementsByTagName(tag)[0]
        if patternSnap.match(property.firstChild.data):
            property.firstChild.replaceWholeText(str(property.firstChild.data).replace('-SNAPSHOT', ''))
            return(str(property.firstChild.data).replace('-SNAPSHOT',''))
        else:
            return property.firstChild.data

    def __setType(self):
        try:
            packaging = self.pom.getElementsByTagName("packaging")[0]
            return packaging.firstChild.data
        except:
            return "jar"

    def getType(self):
        return self.type

    def __setDependency(self,dependency,version):
        pass

    def removeSnapshot(self):
        pattern = re.compile(".*-SNAPSHOT")
        for dependency in self.dependencies:
            if pattern.match(self.dependencies[dependency]['version']):
                self.__setDependency(dependency,self.dependencies[dependency]['version'])

    def _setModules(self):
        modules = []
        childrens = self.pom.getElementsByTagName("modules")[0].childNodes
        for child in childrens:
            try:
                modules.append(child.firstChild.data)
            except:
                pass
        return modules

    def getModules(self):
        return self.modules