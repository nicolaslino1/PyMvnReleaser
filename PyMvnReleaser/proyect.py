from .pom import Pom
from .gitRepoManagment import Git
import logging
import subprocess

class Proycet(Git,Pom):

    def __init__(self,repoUrl, Artifact):
        Git.__init__(self,repoUrl)
        Pom.__init__(self,self.path)
        self.artifactSearch = Artifact

    def build(self):
        self.gitCommit()
        self.gitPush()
        logging.info(msg="buildeando: " + self.getArtifactId())

        process = subprocess.Popen(['mvn', 'release:prepare', 'release:perform', '--batch-mode'], stdout=subprocess.PIPE)
        print(process.args)
        while process.poll() is None:
            if process.stdout.readline().decode("utf-8") != '':
                logging.info(msg="maven_build:  " + process.stdout.readline().decode("utf-8"))

    def testBuild(self):
        unBuildGroup = {}
        for dependency in self.getDependencies():
            if self.artifactSearch.getStatusCode(self.dependencies[dependency]['groupId'],
                                                     dependency,self.dependencies[dependency]['version']) != 200:
                unBuildGroup[dependency] = self.dependencies[dependency]

        if unBuildGroup:
            logging.info(msg="unBuild: " + self.getArtifactId())
            return unBuildGroup
        else:
            return 0


