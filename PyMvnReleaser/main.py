
from .configuration import Configuration
from .proyect import Proycet
from .artifact import Artifact
import logging
from sys import exit


class Main():

    def __init__(self,configFile):
        self.config = Configuration(configFile)
        self.Artifact = Artifact(self.config)
        self.libs = {}
        self.buildLIbsList = []
        self.root_proyect = ''
        self.__instanceProyects()


    def __instanceProyects(self):
        self.root_proyect = Proycet(self.config.getConfigs('root_projects')[0]['repo'], self.Artifact)
        for p in self.config.getConfigs('libs'):
            self.libs[p['name']] = Proycet(p['repo'],self.Artifact)

    def buildRoot(self):
        self.buildLibs(self.root_proyect)

    def prepareBuildLib(self,dependencies):
        for dependency in dependencies:
            for lib in self.libs:
                if self.libs[lib].getType() == 'pom':
                    if dependencies[dependency]['groupId'] == self.libs[lib].getGroupId() \
                            and dependency in self.libs[lib].getModules() \
                                and dependencies[dependency]['version']+'-SNAPSHOT' == self.libs[lib].artifactVersion:
                        if not lib in self.buildLIbsList:
                            self.buildLibs(self.libs[lib])
                            self.buildLIbsList.append(lib)
                elif dependency == self.libs[lib].getArtifactId() \
                        and dependencies[dependency]['version'] + '-SNAPSHOT' == self.libs[lib].artifactVersion:
                    if not lib in self.buildLIbsList:
                        self.buildLibs(self.libs[lib])
                        self.buildLIbsList.append(lib)
                else:
                    logging.info(msg="Dependencia no encontrrada: " + str(dependencies[dependency]))



    def buildLibs(self, objectLib):
        dependenciesForBuild = objectLib.testBuild()
        if dependenciesForBuild != 0:
            self.prepareBuildLib(dependenciesForBuild)
            objectLib.build()
        else:
            objectLib.build()
